package test.data_structures;

import model.data_structures.LinkedList;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestLinkedList {

	private LinkedList<String> arreglo;
	private static int TAMANO=100;
	
	@Before
	public void setUp1() {
		arreglo= new LinkedList<String>();
	}

	public void setUp2() {
		for(int i =0; i< TAMANO; i++){
			arreglo.agregar(""+i);
		}
	}

	@Test
	public void testLinkedList() 
	{
		int n= arreglo.darTamano();
		assertTrue(arreglo!=null);
		assertEquals(0, n);
	}

	@Test
	public void testDarElemento() {
		setUp2();
		
		assertEquals("0", arreglo.darElemento(0));
		assertEquals("1", arreglo.darElemento(1));
		assertEquals(""+(TAMANO-1), arreglo.darElemento(TAMANO-1));
	}
	@Test
	public void testEliminar() {
		setUp2();
		arreglo.eliminar("2");
		assertEquals("3", arreglo.darElemento(2));
	}
	@Test
	public void testAgregar() 
	{
		setUp2();
		
		assertEquals("0", arreglo.darElemento(0));
	}
	

}
