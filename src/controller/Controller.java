package controller;

import java.util.Scanner;

import model.logic.MVCModelo;
import view.MVCView;

public class Controller<T extends Comparable<T>>
{

	/* Instancia del Modelo*/
	private MVCModelo modelo;
	
	/* Instancia de la Vista*/
	private MVCView view;
	
	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}
		
	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String dato = "";
		String dato2="";
		String respuesta = "";

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
				case 1:
					System.out.println("--------- \nCargando...");
				    //int capacidad = lector.nextInt();
				    modelo = new MVCModelo(); 
				    modelo.cargar();
					System.out.println("Lista de CSV cargada");
					System.out.println("Numero actual de elementos " + modelo.darTamano() + "\n---------");						
					break;

				case 2:
					System.out.println("--------- \nDar total viajes reportados en el semestre ");
					//dato = lector.next();
					//modelo.agregar(dato);
					System.out.println("Numero actual de viajes " + modelo.darTamano() + "\n---------");						
					break;

				case 3:
					System.out.println("--------- \nDar total viajes reportados para mes: ");
					dato = lector.next();
					respuesta = modelo.totalViajesReportadosMes(dato);
					//(String) modelo.buscar(dato);
					if ( respuesta != null)
					{
						System.out.println(respuesta);
					}
					else
					{
						System.out.println("Dato NO encontrado");
					}
					System.out.println("Numero actual de elementos " + modelo.darTamano() + "\n---------");						
					break;

				case 4:
					System.out.println("--------- \nDar total viajes reportados para mes: ");
					dato = lector.next();
					System.out.println("--------- \nDar total viajes reportados para origen: ");
					dato2 = lector.next();
					respuesta = modelo.totalViajes(dato, dato2);
					//modelo.eliminar(dato);
					if ( respuesta != null)
					{
						System.out.println( respuesta);
					}
					else
					{
						System.out.println("Dato NO eliminado");							
					}
					System.out.println("Numero actual de elementos " + modelo.darTamano() + "\n---------");						
					break;
					
				case 5:
					System.out.println("--------- \nDar cadena (simple) a eliminar: ");
					dato = lector.next();
					dato2=lector.next();
					respuesta = modelo.totalViajes(dato, dato2);
					if ( respuesta != null)
					{
						System.out.println(respuesta);
					}
					else
					{
						System.out.println("Dato NO eliminado");							
					}
					System.out.println("Numero actual de elementos " + modelo.darTamano() + "\n---------");						
					break;

				case 6: 
					System.out.println("--------- \nContenido del Arreglo: ");
					view.printModelo(modelo);
					System.out.println("Numero actual de elementos " + modelo.darTamano() + "\n---------");						
					break;	
					
				case 7: 
					System.out.println("--------- \n Hasta pronto !! \n---------"); 
					lector.close();
					fin = true;
					break;	
					

				default: 
					System.out.println("--------- \n Opcion Invalida !! \n---------");
					break;
			}
		}
		
	}	
}
