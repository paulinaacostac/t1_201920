package model.data_structures;

public class Node<T extends Comparable<T>> 
{
	private T elemento;
	private Node<T> siguiente;

	public Node(T pElemento)
	{
		elemento = pElemento;
		this.siguiente=null;
	}
	public Node<T> darSiguiente()
	{
		return siguiente;
	}
	public T darElemento() 
	{
		return elemento;
	}
	public void cambiarSiguiente(Node<T> nodo)
	{
		siguiente=nodo;
	}
}
