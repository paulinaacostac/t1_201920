package model.data_structures;

/**
 * 2019-01-23
 * Estructura de Datos Arreglo Dinamico de Strings.
 * El arreglo al llenarse (llegar a su maxima capacidad) debe aumentar su capacidad.
 * @author Fernando De la Rosa
 *
 */
public class ArregloDinamico <T extends Comparable<T>> implements IArregloDinamico {
		/**
		 * Capacidad maxima del arreglo
		 */
        private int tamanoMax;
		/**
		 * Numero de elementos presentes en el arreglo (de forma compacta desde la posicion 0)
		 */
        private int tamanoAct;
        /**
         * Arreglo de elementos de tamaNo maximo
         */
        private T elementos[ ];
        private LinkedList elemento;

        /**
         * Construir un arreglo con la capacidad maxima inicial.
         * @param max Capacidad maxima inicial
         */
		public ArregloDinamico( int max )
        {
               elementos = (T[]) new String[max];
               tamanoMax = max;
               tamanoAct = 0;
        }
        
		public void agregar( Object dato )
        {
               if ( tamanoAct == tamanoMax )
               {  // caso de arreglo lleno (aumentar tamaNo)
                    tamanoMax = 2 * tamanoMax;
                    String [ ] copia = (String[]) elementos;
                    elementos = (T[]) new String[tamanoMax];
                    for ( int i = 0; i < tamanoAct; i++)
                    {
                     	 elementos[i] = (T) copia[i];
                    } 
            	    System.out.println("Arreglo lleno: " + tamanoAct + " - Arreglo duplicado: " + tamanoMax);
               }	
               elementos[tamanoAct] = (T) dato;
               tamanoAct++;
       }

		public Integer darCapacidad() {
			return tamanoMax;
		}

		public Integer darTamano() 
		{
			return tamanoAct;
		}

		public T darElemento(Integer i) 
		{
			// TODO implementar
			return null;
		}

		public Comparable buscar(Object dato) {
			// TODO implementar
			return null;
			// Recomendacion: Usar el criterio de comparacion natural (metodo compareTo()) definido en Strings.
		}

		public Comparable eliminar(Object dato) {
			// TODO implementar
			return null;
			// Recomendacion: Usar el criterio de comparacion natural (metodo compareTo()) definido en Strings.
		}


	


}
