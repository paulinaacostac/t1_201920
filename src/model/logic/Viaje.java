/*
 * 
 */
package model.logic;

	

public class Viaje implements Comparable<Viaje>
{
	private double sourceid;
	private double dstid;
	private double month;
	private double meantraveltime;
	private double standarddeviationtraveltime;
	private double geometricmeantraveltime;
	private double geometricstandarddeviationtraveltime;
	
	public Viaje (double psourceid, double pdstid, double pmonth,double pmeantraveltime, double pstandarddeviationtraveltime, double pgeometricmeantraveltime, double pgeometricstandarddeviationtraveltime )
	{
		sourceid=psourceid;
		dstid=pdstid;
		month=pmonth;
		meantraveltime=pmeantraveltime;
		standarddeviationtraveltime=pstandarddeviationtraveltime;
		geometricmeantraveltime=pgeometricmeantraveltime;
		geometricstandarddeviationtraveltime=pgeometricstandarddeviationtraveltime;
		
	}
	public String darSourceid()
	{
		return String.valueOf(sourceid);
	}
	public String darDstid()
	{
		return String.valueOf(dstid);
	}
	public String darMonth()
	{
		return String.valueOf(month);
	}
	public String darMeanTraveltTime()
	{
		return String.valueOf(meantraveltime);
	}
	public String darStandardDeviationTravelTime()
	{
		return String.valueOf(standarddeviationtraveltime);
	}
	public String darGeometricMeanTravelTime()
	{
		return String.valueOf(geometricmeantraveltime);
	}
	public String darGeometricStandardDeviationTravelTime()
	{
		return String.valueOf(geometricstandarddeviationtraveltime);
	}
	@Override
	public int compareTo(Viaje o) {
		// TODO Auto-generated method stub
		return 0;
	}



}
