package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.opencsv.CSVReader;

import model.data_structures.ArregloDinamico;
import model.data_structures.IArregloDinamico;
import model.data_structures.LinkedList;

/**
 * Definicion del modelo del mundo
 * @param <T>
 *
 */
public class MVCModelo<T> 
{
	/**
	 * Atributos del modelo del mundo
	 */
	private LinkedList<Viaje> lista;
	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public MVCModelo()
	{
		lista = new LinkedList<Viaje>();
	}

	/**
	 * Constructor del modelo del mundo con capacidad dada
	 * @param tamano
	 */
	public MVCModelo(String[] args)
	{
		lista=new LinkedList<Viaje>();
	}
	public void cargar()
	{
		CSVReader reader = null;
		try 
		{

			reader = new CSVReader(new FileReader("./data/bogota-cadastral-2018-1-All-MonthlyAggregate.csv"));
			reader.skip(1);
			for(String[] nextLine : reader) 
			{
				Viaje viaje = new Viaje(Double.parseDouble(nextLine[0]), Double.parseDouble(nextLine[1]), Double.parseDouble(nextLine[2]), Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]), Double.parseDouble(nextLine[5]), Double.parseDouble(nextLine[6])); 
				lista.agregar(viaje);
				//System.out.println("col1: " + nextLine[0] + ", col2: "+ nextLine[1]+ ", col3: "+ nextLine[2]+ ", col4: "+ nextLine[3]+ ", col5: "+ nextLine[4]+ ", col6: "+ nextLine[5]+ ", col7: "+ nextLine[6]);
			}
			reader = new CSVReader(new FileReader("./data/bogota-cadastral-2018-2-All-MonthlyAggregate.csv"));
			reader.skip(1);
			for(String[] nextLine : reader) 
			{
				Viaje viaje = new Viaje(Double.parseDouble(nextLine[0]), Double.parseDouble(nextLine[1]), Double.parseDouble(nextLine[2]), Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]), Double.parseDouble(nextLine[5]), Double.parseDouble(nextLine[6])); 
				lista.agregar(viaje);
				//System.out.println("col1: " + nextLine[0] + ", col2: "+ nextLine[1]+ ", col3: "+ nextLine[2]+ ", col4: "+ nextLine[3]+ ", col5: "+ nextLine[4]+ ", col6: "+ nextLine[5]+ ", col7: "+ nextLine[6]);
			}
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
		try {
			reader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * Servicio de consulta de numero de elementos presentes en el modelo 
	 * @return numero de elementos presentes en el modelo
	 */
	public int darTamano()
	{
		return lista.darTamano();
	}

	/**
	 * Requerimiento de agregar dato
	 * @param <T>
	 * @param dato
	 */
	public void agregar(T dato)
	{	
		lista.agregar((Viaje) dato);
	}

	/**
	 * Requerimiento buscar dato
	 * @param dato Dato a buscar
	 * @return dato encontrado
	 */
	public String buscar(Viaje dato)
	{
		return lista.buscar(dato).darSourceid();

	}

	/**
	 * Requerimiento eliminar dato
	 * @param dato Dato a eliminar
	 * @return dato eliminado
	 */
	public String eliminar(Viaje dato)
	{
		return lista.eliminar(dato).darDstid();
	}
	public String totalViajesReportadosMes(String mes)
	{
		int rta=0;
		int contador=0;
		for (int i=0;i<lista.darTamano();i++)
		{
			if (lista.darElemento(i).darMonth().equals(mes))
			{
				contador++;
			}
			rta=contador;
		}
		return Integer.toString(rta); //+ "Porcentaje: "+Integer.toString(contador/lista.darTamano()*100)+"%"

	}
	public String totalViajes(String mes, String origen)
	{
		int contador=0;
		for (int i=0;i<lista.darTamano();i++)
		{
			if (lista.darElemento(i).darMonth().equals(mes) && lista.darElemento(i).darSourceid().equals(origen))
			{
				contador++;
			}
		}
		return "Viajes reportados para mes " + mes+ " y origen "+origen+" : "+Integer.toString(contador)+ "Porcentaje: "+Integer.toString(contador/lista.darTamano()*100)+"%";
	}

	public String buscarViajes(String mes, String origen)
	{
		String rta="";
		for (int i=0;i<lista.darTamano();i++)
		{
			if (lista.darElemento(i).darMonth().equals(mes) && lista.darElemento(i).darSourceid().equals(origen))
			{
				rta+="sourceid"+lista.darElemento(i).darSourceid()+"dstid"+lista.darElemento(i).darDstid()+"mean_travel_time"+lista.darElemento(i).darMeanTraveltTime()+"standard deviation standard time"+lista.darElemento(i).darStandardDeviationTravelTime()+"geometric mean travel time"+lista.darElemento(i).darGeometricMeanTravelTime();
			}
		}
		return rta;
	}





}
